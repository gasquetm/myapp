FROM python:3.6-alpine

ENV USERNAME default

WORKDIR /root

COPY app.py .

RUN chmod u+x app.py

CMD ["python3", "/root/app.py"]
